#!/bin/sh
# vim:sw=4:ts=4:et

set -e

# Function to log messages
entrypoint_log() {
    if [ -z "${CADDY_ENTRYPOINT_QUIET_LOGS:-}" ] || [ -z "${HTTPD_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

# Function to handle and forward signals
forward_signal() {
    entrypoint_log "$0: Received signal $1, forwarding to PID $child_pid"
    kill -s "$1" "$child_pid"
}

if [ "$1" = "caddy" ] || [ "$1" = "httpd" ]; then
    if find "/docker-entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read v; then
        entrypoint_log "$0: /docker-entrypoint.d/ is not empty, will attempt to perform configuration"

        entrypoint_log "$0: Looking for shell scripts in /docker-entrypoint.d/"
        find "/docker-entrypoint.d/" -follow -type f -print | sort -V | while read -r f; do
            case "$f" in
                *.envsh)
                    if [ -x "$f" ]; then
                        entrypoint_log "$0: Sourcing $f";
                        . "$f"
                    else
                        # warn on shell scripts without exec bit
                        entrypoint_log "$0: Ignoring $f, not executable";
                    fi
                    ;;
                *.sh)
                    if [ -x "$f" ]; then
                        entrypoint_log "$0: Launching $f";
                        "$f"
                    else
                        # warn on shell scripts without exec bit
                        entrypoint_log "$0: Ignoring $f, not executable";
                    fi
                    ;;
                *) entrypoint_log "$0: Ignoring $f";;
            esac
        done

        entrypoint_log "$0: Configuration complete; ready for start up"
    else
        entrypoint_log "$0: No files found in /docker-entrypoint.d/, skipping configuration"
    fi
fi

# Start the command in the background
"$@" &
child_pid=$!

# Trap signals and forward them to the child process
trap 'forward_signal TERM' TERM
trap 'forward_signal INT' INT
trap 'forward_signal QUIT' QUIT

# Wait for the child process to exit
wait "$child_pid"
exit_code=$?

exit "$exit_code"