#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# Set the specific directory to search in
search_dir="${HTML_DIRECTORY}"

# Set the specific URL prefix
url_prefix=$(echo "${CONTEXT_URL}" | sed -E 's@(://)|/$|((/)/+)@\1\3@g')

# Set the target directory for finding the actual files
target_dir="${HTML_DIRECTORY}"

# Use find and grep to search for files containing the patterns
find "$search_dir" -type f -name '*.html' -o -name '*.htm' | while read -r file; do
  grep -Eo '<(script|link)([^>]*(src|href)="(('"$url_prefix"'|[^:/])[^\"]*)[^>]*integrity=[^>]*|[^>]*integrity=[^>]*(src|href)="(('"$url_prefix"'|[^:/])[^\"]*)[^>]*)[^>]*>' "$file" | while read -r line; do
    # Extract the URL
    url=$(echo "$line" | grep -Eo '(src|href)="([^"]*)"' | cut -d '"' -f2)

    # Trim the URL prefix if it exists
    trimmed_url=${url#$url_prefix}

    # Find the corresponding file in the target directory
    file_path="$target_dir/$trimmed_url"

    if ! test -f "${file_path}"; then
      break
    fi

    # Determine the hash command based on the desired bits
    case "${PATCH_INTEGRITY_BITS}" in
      256)
        hash_cmd="sha256sum"
        ;;
      512)
        hash_cmd="sha512sum"
        ;;
      *)
        echo "Unsupported hash length: ${PATCH_INTEGRITY_BITS}" >&2
        exit 1
        ;;
    esac

    # Generate a new subresource integrity hash
    new_hash=$($hash_cmd "$file_path" | awk '{print $1}' | xxd -r -p | base64 -w 0)

    # Replace the old hash with the new hash
    old_hash=$(echo "$line" | grep -Eo 'integrity="[^"]*"' | cut -d '"' -f2)

    sed -i -E 's@('"${trimmed_url}"'"?)([^>]*)(integrity="?[^"> ]*"?)@\1 integrity="sha'"${PATCH_INTEGRITY_BITS}"'-'"${new_hash}"'"\2@g' $file
  done
done
