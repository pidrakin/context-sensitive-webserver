#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

find "${HTML_DIRECTORY}" -not -name "*.gz" -type f -exec gzip -k -9 -f {} \;