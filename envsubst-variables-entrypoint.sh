#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# find files with variables
variableFiles=$(grep -lrE "$(env | grep "^${ENVSUBST_PREFIX}" | sed -E "s/(\w+)=.*/\\\\$\\\\{\1\\\\}/g" | tr "\n" "|" | sed 's/|$//')" ${HTML_DIRECTORY})

# replace variables in files
for f in ${variableFiles}; do
  envsubst "$(env | grep "^${ENVSUBST_PREFIX}" | sed -E "s/(\w+)=.*/\${\1}/g" | tr "\n" "," | sed 's/,$//')" < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"
done
