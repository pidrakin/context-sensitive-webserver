#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

set -eu

download() {
    url="$1"
    if command -v curl >/dev/null 2>&1; then
        # Use curl
        curl --silent --output /dev/null --write-out "%{http_code}" "$url"
    elif command -v wget >/dev/null 2>&1; then
        # Use wget
        status_code=$(wget --server-response --spider "$url" 2>&1 | awk '/^  HTTP\// {print $2}' | tail -n1)
        echo "$status_code"
    else
        echo "Error: Neither curl nor wget is installed" >&2
        return 1
    fi
}

statuscode=$(download http://localhost:8080)
test $statuscode -eq 200

if [ ${PROBE_TARGET_SCRIPT} ]; then

  # Determine the hash command based on the desired bits
  case "${PATCH_INTEGRITY_BITS}" in
    256)
      hash_cmd="sha256sum"
      ;;
    512)
      hash_cmd="sha512sum"
      ;;
    *)
      echo "Unsupported hash length: ${PATCH_INTEGRITY_BITS}" >&2
      exit 1
      ;;
  esac

  script=$(ls ${HTML_DIRECTORY} | grep -e "${PROBE_TARGET_SCRIPT}")
  integrity=$(sed -E 's@'${script}'[^i]*integrity="?sha'${PATCH_INTEGRITY_BITS}'-([^"> ]*)"?@\nDGST: \1\n@g' ${HTML_DIRECTORY}/index.html | awk '/DGST:/ { print $2 }')
  if [ ${integrity} ]; then
    echo $integrity | base64 -d | xxd -p | awk '{ printf "%s", $0 } END { print "  '${HTML_DIRECTORY}'/'$script'" }' | $hash_cmd -c - > /dev/null
  fi
fi
