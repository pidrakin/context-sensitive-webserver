server {
  listen 8080;
  root /usr/share/nginx/html;

  # GZIP Compression - In case Brotli is not supported
  gzip on;
  gzip_vary on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_types *;

  # Brotli Compression
  brotli on;
  brotli_static off;
  brotli_min_length 20;
  brotli_comp_level 6;
  brotli_types *;

  location ~* \.(jpg|jpeg|png|gif|swf|svg|ico|mp4|eot|ttf|otf|woff|woff2|css|js)$ {
      add_header Cache-Control "max-age=86400, must-revalidate, s-maxage=2592000";
  }
  location / {
    index index.html index.htm;
    expires -1;
    add_header Pragma "no-cache";
    add_header Cache-Control "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
    try_files $uri $uri/ /index.html =404;

    include /etc/nginx/conf.d/default.d/*.conf;
  }
}
