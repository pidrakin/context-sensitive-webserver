#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

find "${HTML_DIRECTORY}" -name "*.gz" -type f -exec gzip -d -f {} \;