ARG NGINX_VERSION=1.27.1
FROM nginx:${NGINX_VERSION}-alpine as builder

ARG ENABLED_MODULES="brotli ndk set-misc"

SHELL ["/bin/ash", "-exo", "pipefail", "-c"]

RUN set -ex \
    && if [ "$ENABLED_MODULES" = "" ]; then \
        echo "No additional modules enabled, exiting"; \
        exit 1; \
    fi

RUN apk --no-cache add linux-headers openssl-dev pcre2-dev zlib-dev openssl abuild \
               musl-dev libxslt libxml2-utils make mercurial gcc unzip git \
               xz g++ coreutils \
    # allow abuild as a root user \
    && printf "#!/bin/sh\\nSETFATTR=true /usr/bin/abuild -F \"\$@\"\\n" > /usr/local/bin/abuild \
    && chmod +x /usr/local/bin/abuild \
    && hg clone -r ${NGINX_VERSION}-${PKG_RELEASE} https://hg.nginx.org/pkg-oss/ \
    && mkdir /tmp/packages \
    && for module in $ENABLED_MODULES; do \
        echo "Building $module for nginx-$NGINX_VERSION"; \
        if make -C /pkg-oss/alpine list | grep -E "^$module\s+\d+" > /dev/null; then \
            echo "Building $module from pkg-oss sources"; \
            make -C /pkg-oss/alpine abuild-module-$module BASE_VERSION=$NGINX_VERSION NGINX_VERSION=$NGINX_VERSION; \
            apk --no-cache add $(. /pkg-oss/alpine/abuild-module-$module/APKBUILD; echo $makedepends;); \
            make -C /pkg-oss/alpine module-$module BASE_VERSION=$NGINX_VERSION NGINX_VERSION=$NGINX_VERSION; \
            find ~/packages -type f -name "*.apk" -exec mv -v {} /tmp/packages/ \;; \
            BUILT_MODULES="$BUILT_MODULES $module"; \
        else \
            echo "Don't know how to build $module module, exiting"; \
            exit 1; \
        fi; \
    done \
    && echo "BUILT_MODULES=\"$BUILT_MODULES\"" > /tmp/packages/modules.env

FROM nginxinc/nginx-unprivileged:${NGINX_VERSION}-alpine-slim

USER root

RUN --mount=type=bind,target=/tmp/packages/,source=/tmp/packages/,from=builder \
    . /tmp/packages/modules.env \
    && for module in $BUILT_MODULES; do \
           apk add --no-cache --allow-untrusted /tmp/packages/nginx-module-"${module}"-"${NGINX_VERSION}"*.apk; \
       done \
    && printf 'load_module modules/ngx_http_brotli_static_module.so;\n\
load_module modules/ngx_http_brotli_filter_module.so;\n\
load_module modules/ndk_http_module.so;\n\
load_module modules/ngx_http_set_misc_module.so;\n\n\
%s\n' "$(cat /etc/nginx/nginx.conf)" >/etc/nginx/nginx.conf


RUN chown -R nginx:root /usr/share/nginx/html \
    && chmod g+w /usr/share/nginx/html \
    && rm -rf /usr/share/nginx/html/* \
    && install -v -d -o nginx -g root -m 755 /etc/nginx/conf.d/default.d

COPY --chown=nginx:root default.conf /etc/nginx/conf.d/default.conf
COPY --chown=nginx:root --chmod=0755 ungzip-all-assets-entrypoint.sh /docker-entrypoint.d/50-ungzip-all-assets.sh
COPY --chown=nginx:root --chmod=0755 envsubst-variables-entrypoint.sh /docker-entrypoint.d/51-envsubst-variables.sh
COPY --chown=nginx:root --chmod=0755 patch-context-url-entrypoint.sh /docker-entrypoint.d/52-patch-context-url.sh
COPY --chown=nginx:root --chmod=0755 patch-integrity-hashes-entrypoint.sh /docker-entrypoint.d/53-patch-integrity-hashes.sh
COPY --chown=nginx:root --chmod=0755 gzip-all-assets-entrypoint.sh /docker-entrypoint.d/54-gzip-all-assets.sh
COPY --chown=nginx:root --chmod=0755 startup-probe.sh /

USER nginx

ENV HTML_DIRECTORY=/usr/share/nginx/html
ENV DUMMY_URL=http://dummy-url.tld
ENV CONTEXT_URL=/
ENV PATCH_INTEGRITY_BITS=512
ENV PROBE_TARGET_SCRIPT=

ENV ENVSUBST_PREFIX=SUB_
