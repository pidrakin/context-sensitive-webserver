#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# normalize context url and delimit without a trailing slash
contextURL=$(echo "${CONTEXT_URL}/" | sed -E 's@(://)|((/)/+)@\1\3@g')

# find files that contain dummy url
pageFiles=$(grep -lr "${DUMMY_URL}" ${HTML_DIRECTORY})

# loop all files with the dummy url and patch them with the real context url
for f in ${pageFiles}; do
  sed -i -E 's@base href="?'${DUMMY_URL}'"?@base href="'${contextURL}'"@g' "$f"
  # sed -i -E "s@${DUMMY_URL}/?@${contextURL}/@g" "$f"
done

# # find files that contain relative URLs
# linkFiles=$(grep -rlE ' (src|href)="?' ${HTML_DIRECTORY})

# # loop through all files and patch relative URLs with the context URL
# for f in ${linkFiles}; do
#   # extract lines with relative URLs
#   relativeLinks=$(grep -E ' (src|href)="?' "$f" | grep -vE 'https?://|base href')

#   # loop through each relative link and replace it with the prefixed version
#   echo "$relativeLinks" | while IFS= read -r line; do
#     if [ -z "$line" ]; then
#       continue
#     fi
#     # extract the URL part from the line
#     urls=$(echo "$line" | grep -oE '(src|href)="?[^ "]+"?')
#     echo "$urls" | uniq | while IFS= read -r url; do
#       # extract the URL part from the line
#       url=$(echo "$url" | sed -E 's@(src|href)="?([^ "]+)"?@\2@')
#       prefixed_url=$(echo "${url}" | sed -E 's@(://)|((/)/*)@\1\3@g')
#       # replace the relative URL with the prefixed version
#       sed -i -E "s@${url}@${prefixed_url}@g" "$f"
#     done
#   done
# done
